

# imports
import numpy as np
import matplotlib.pyplot as plt
import os, copy, time, pickle

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from torchvision import datasets, models, transforms, utils

# set up the workspace
reseed = lambda: np.random.seed(seed=0) ; ms = torch.manual_seed(0) # for reproducibility
reseed()

os.mkdir('figures') if not os.path.exists('figures') else None
os.mkdir('saved') if not os.path.exists('saved') else None


class MnistLoader():
    def __init__(self):
        self.modes = modes = ['train', 'test']
        trans = transforms.Compose([transforms.ToTensor(), ])  # transforms.Normalize((0.1307,), (0.3081,))
        self.dsets = {k: datasets.MNIST('./data_local', train=k == 'train', download=True, transform=trans) for k in modes}
        self.loaders = {m: iter([]) for m in modes}
        self.batch_size = None

    def next(self, batch_size, mode='train', sigma=0):
        if batch_size is not None and batch_size != self.batch_size:
            self.loaders[mode] = iter(torch.utils.data.DataLoader(self.dsets[mode], batch_size, shuffle=True))
            self.batch_size = batch_size
        data = next(self.loaders[mode], None)
        if data is None or batch_size != data[0].shape[0]:
            self.loaders[mode] = iter(torch.utils.data.DataLoader(self.dsets[mode], batch_size, shuffle=True))
            data = next(self.loaders[mode], None)
        return data[0].view(self.batch_size, 28 ** 2).numpy(), data[1].numpy()


def onehot(values, n_values):  # converts to one-hot (use np.argmax to convert back)
    print(values)
    print(type(values))
    print(values.size)
    print(values.shape)
    print(n_values)
    oh = np.zeros((values.size, n_values))
    oh[range(values.size), values.ravel()] = 1
    return oh.reshape(list(values.shape) + [10])


def accuracy(model, dataloader, samples=10000, mode='test'):
    X, y = dataloader.next(samples, mode=mode)
    model.eval()  # put model in evaluation mode
    logits = model.forward(X)  # logits should be a numpy array
    print('----------------')
    print(logits[0])
    print(logits.argmax(1)[0])
    model.train()  # put model in train mode
    y = onehot(y, logits.shape[-1])
    return 100 * sum(y.argmax(1) == logits.argmax(1)) / samples  # assumes y and y_hat are numpy arrays


class DenseMnistClassifier(nn.Module):
    def __init__(self, hdim=200):
        super(DenseMnistClassifier, self).__init__()
        self.linear1 = nn.Linear(28**2, hdim)
        self.linear2 = nn.Linear(hdim, hdim)
        self.linear3 = nn.Linear(hdim, 10)

    def forward(self, x):
        x = Variable(torch.Tensor(x)) if type(x) == np.ndarray else x # maybe convert to an autograd variable
        h1 = torch.sigmoid(self.linear1(x))  # shape [batch x 28^2] -> [batch x hdim]
        h2 = torch.sigmoid(self.linear2(h1)) # shape [batch x hdim] -> [batch x hdim]
        h3 = self.linear3(h2) # shape [batch x hdim] -> [batch x 10  ]
        return h3 if self.training else h3.data.numpy() # maybe return as a numpy array, else as a Variable



def to_pickle(thing, name): # save something
    with open('saved/{}.pkl'.format(name), 'wb') as handle:
        pickle.dump(thing, handle, protocol=pickle.HIGHEST_PROTOCOL)

def from_pickle(name): # load something
    thing = None
    with open('saved/{}.pkl'.format(name), 'rb') as handle:
        thing = pickle.load(handle)
    return thing

running_mean = lambda mean, update: update if mean is None else .99*mean + (1-.99)*update

info_dict = {'hidden_dim': 200,
        'lr': 1e-2,
        'batch_size': 128,
        'test_every': 100,
        'global_step': 0,
        'epochs': 3,
        'loss': [],
        'accuracy': [],
        'name': 'mnist-fc'}
info_dict['total_steps'] = int(info_dict['epochs']*60000/info_dict['batch_size'])

class ObjectView(object):
    def __init__(self, d): self.__dict__ = d
info = ObjectView(info_dict)

dataloader = MnistLoader()
model = DenseMnistClassifier(info.hidden_dim)
optimizer = torch.optim.Adam(model.parameters(), lr=info.lr)
model.train()  # put model in train mode

#try:
#    [model, info_dict] = from_pickle(info.name)
#    info = ObjectView(info_dict)
#    print('\t* loaded a model')
#except:
#    print('\t* no model to load')

acc = accuracy(model, dataloader, mode='test')
print('test accuracy: {:.2f}%'.format(acc))


start_t = print_t = time.time()
run_loss = None
test_acc = train_acc = 10
for step in range(info.global_step + 1, info.total_steps):
    X, y = dataloader.next(info.batch_size)  # get a minibatch
    #print(X,type(X))
    #print(type(y),y)
    logits = model(X)  # forward pass
    #print("logits", logits, logits.shape)
    y_hat = F.log_softmax(logits, dim=-1)  # log softmax over logits
    #print("y_hat", y_hat, y_hat.shape)
    #print("y", Variable(torch.LongTensor(y)), torch.LongTensor(y).shape)
    print(y_hat.shape)
    print(Variable(torch.LongTensor(y)).shape)

    exit()
    loss = F.nll_loss(y_hat, Variable(torch.LongTensor(y)))  # this is the loss function
    loss.backward()
    optimizer.step()
    optimizer.zero_grad()  # get grads, apply with optimizer

    info.loss += [loss.data.view(-1)[0]]
    run_loss = running_mean(run_loss, loss.data.view(-1)[0])
    info.global_step = step
    print(run_loss)
    if time.time() - print_t > .2:
        print_t = time.time()
        print('\tstep {}/{} | elapsed {:.1f}s | loss {:.4f} | test_acc {:.2f}% | train_acc {:.2f}%'
              .format(step, info.total_steps, time.time() - start_t, run_loss, test_acc, train_acc), end='\r')
    if step % info.test_every == 0 or step == info.total_steps - 1:
        test_acc = accuracy(model, dataloader, samples=10000, mode='test')
        train_acc = accuracy(model, dataloader, samples=10000, mode='train')
        info.accuracy += [[test_acc, train_acc].copy()]

        to_pickle([model, info_dict], info.name)  # save model and train stats


acc = accuracy(model, dataloader, mode='test')
print('test accuracy: {:.2f}%'.format(acc))