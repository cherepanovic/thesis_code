from pytorch_transformers import BertModel, XLNetModel
from rest.run_glue import *

if False:
    tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
    model = BertModel.from_pretrained('bert-base-uncased')
    model.eval()

    torch.manual_seed(0)
    sent = "this is a complicated sentence"
    tokens = ['[CLS]'] + tokenizer.tokenize(sent)
    ids = tokenizer.convert_tokens_to_ids(tokens)
    t = torch.LongTensor([ids])

    with torch.no_grad():
        out = model(t)[0]

    torch.manual_seed(0)
    sent = "this is a complicated sentence"
    tokens = ['[CLS]'] + tokenizer.tokenize(sent)
    tokens.extend(['[PAD]'] * 3)
    ids = torch.tensor(tokenizer.convert_tokens_to_ids(tokens)).unsqueeze(0)
    mask = torch.zeros((1, ids.shape[1], ids.shape[1]), dtype=torch.float)
    mask[:, :, 0:-3] = 1.0

    with torch.no_grad():
        out2 = model(ids, attention_mask = mask[:, 0])[0]

    print('------------')
    for i in range(out.shape[1]):
        print(i, out[0][0, i].item())

    print('------------')
    for i in range(out2.shape[1]):
        torch.manual_seed(0)
        print(i, out2[0][0, i].item())

    '''
    ------------
    0 -0.25838226079940796
    1 0.44730544090270996
    2 -0.3153173625469208
    3 0.1720467507839203
    4 0.4655050039291382
    5 0.5566251277923584
    ------------
    0 -0.25838184356689453
    1 0.44730594754219055
    2 -0.31531691551208496
    3 0.17204701900482178
    4 0.4655049443244934
    5 0.556624710559845
    6 0.23085300624370575
    7 -0.2919197082519531
    8 -0.4632873237133026
    '''
if True:
    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
    model = XLNetModel.from_pretrained('xlnet-base-cased')
    model.eval()

    torch.manual_seed(0)
    sent = "Hello, my dog is cute"
    tokens = tokenizer.tokenize(sent)
    ids = tokenizer.convert_tokens_to_ids(tokens)
    t = torch.LongTensor([ids])

    with torch.no_grad():
        out = model(t)[0]

    sent = "Hello, my dog is cute"
    tokens = tokenizer.tokenize(sent)
    tokens.extend(['<pad>'] * 3)
    ids = torch.tensor(tokenizer.convert_tokens_to_ids(tokens)).unsqueeze(0)
    mask = torch.zeros((1, ids.shape[1], ids.shape[1]), dtype=torch.float)
    mask[:, :, 0:-3] = 1.0

    with torch.no_grad():
        out2 = model(ids, attention_mask=mask[:, 0])[0]

    sent = "Hello, my dog is cute <pad>"
    tokens = tokenizer.tokenize(sent)
    ids = tokenizer.convert_tokens_to_ids(tokens)
    t = torch.LongTensor([ids])

    with torch.no_grad():
        out_3 = model(t)[0]

    print('------------')
    for i in range(out.shape[1]):
        print(i, out[0][0, i].item())

    print('------------')
    for i in range(out2.shape[1]):
        torch.manual_seed(0)
        print(i, out2[0][0, i].item())

    print('------------')
    for i in range(out_3.shape[1]):
        torch.manual_seed(0)
        print(i, out_3[0][0, i].item())
    '''
------------
0 -0.7767028212547302
1 -1.5196354389190674
2 -3.1579976081848145
3 1.5723580121994019
4 -2.626596212387085
5 1.6940643787384033
6 1.3458458185195923
------------
0 -0.7767004370689392
1 -1.5196330547332764
2 -3.1579959392547607
3 1.5723563432693481
4 -2.6265885829925537
5 1.69405996799469
6 1.345845103263855
7 0.2612830102443695
8 -1.5281519889831543
9 -0.36109429597854614
------------
0 -1.522645354270935
1 -2.920607089996338
2 -2.243230104446411
3 1.4940992593765259
4 -1.36550772190094
5 0.6392733454704285
6 0.5941150784492493
7 -0.6187223196029663
    '''
