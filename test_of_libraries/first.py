import torch

import gpytorch
from pytorch_transformers import XLNetTokenizer, XLNetModel


from torch.utils.data import Dataset, DataLoader
import pandas as pd
from data.datareader import Csvdataset
from auxdir.poolings import MaxPoolingChannel

class GPReg(gpytorch.models.ExactGP):
    def __init__(self, train_x, train_y, likelihood, feature_extractor=None):
        super(GPReg, self).__init__(train_x, train_y, likelihood)
        self.mean_module = gpytorch.means.ZeroMean()
        self.covar_module = gpytorch.kernels.ScaleKernel(gpytorch.kernels.RBFKernel())
        self.feature_extractor = feature_extractor

    def forward(self, x):
        if self.feature_extractor is not None:
            x = self.feature_extractor(x)
        mean_x = self.mean_module(x)
        covar_x = self.covar_module(x)
        return gpytorch.distributions.MultivariateNormal(mean_x, covar_x)


data = pd.read_csv('data/yelp_review_full_csv/train.csv')
data = pd.DataFrame(data)
data_set = Csvdataset(csv_file='data/yelp_review_full_csv/train.csv',
                                    root_dir='data/yelp_review_full_csv/',
                    )
dataloader = DataLoader(data_set, batch_size=4,
                        shuffle=True, num_workers=4)


tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
xlnet_model = XLNetModel.from_pretrained('xlnet-base-cased')

# Encode text
input_ids = torch.tensor([tokenizer.encode(x) for x in ['here I am', 'she is there']])

print(input_ids)

with torch.no_grad():
    last_hidden_states = xlnet_model(input_ids)[0]  # Models outputs are now tuples

    model = torch.nn.Sequential(xlnet_model, MaxPoolingChannel(1))
    res = model(input_ids)

likelihood = gpytorch.likelihoods.GaussianLikelihood()

for i_batch, sample_batched in enumerate(dataloader):
    '''
    gpmodel = GPReg(
        torch.tensor(sample_batched['sample']),
        torch.tensor(sample_batched['label']),
        likelihood,
    )
    gpmodel.feature_extractor = torch.nn.Sequential(xlnet_model, MaxPoolingChannel(1))
    '''
    print(sample_batched['label'])
    print(sample_batched['label'].shape)
    print(len(sample_batched['label']))
    print(type(sample_batched['label']))

    print(sample_batched['sample'])
    print(len(sample_batched['sample'][0]))
    print(len(sample_batched['sample'][1]))
    print(len(sample_batched['sample'][2]))
    print(len(sample_batched['sample'][3]))
    #print(sample_batched['sample'].shape)
    print(len(sample_batched['sample']))
    print(type(sample_batched['sample']))

    #input_ids = torch.tensor([tokenizer.encode(sample_batched['sample'])])

    print(input_ids)
    exit()

'''
optimizer = torch.optim.Adam([
    {'params': gpmodel.feature_extractor.parameters()},
    {'params': gpmodel.covar_module.parameters()},
    {'params': gpmodel.mean_module.parameters()},
    {'params': gpmodel.likelihood.parameters()},
], lr=0.01)
'''
