# imports
import numpy as np
import matplotlib.pyplot as plt

from pytorch_transformers import XLNetModel, XLNetTokenizer
from torch.utils.data import Dataset, DataLoader
import torch
import torch.nn.functional as F
from torch.autograd import Variable

import pandas as pd
import itertools
import os
import sys

import warnings
from collections import OrderedDict
from torch._six import container_abcs
from itertools import islice
import operator


use_cuda = torch.cuda.is_available()
device = None
if use_cuda:
    sys.path.append('/home/cherepanov/thesis/')
    device = torch.device("cuda:1")
else:
    sys.path.append('/home/ic/projects/thesis/thesis_code/thesis_code/')
    device = torch.device('cpu')

from auxdir.sequential import Sequential as Seq
from auxdir.poolings import MaskedMaxPooling
from data.datareader import Dataset, Csvdataset

def onehot(values, n_values):  # converts to one-hot (use np.argmax to convert back)
    print(values)
    oh = np.zeros((values.size, n_values))
    oh[range(values.size), values.ravel()] = 1
    return oh.reshape(list(values.shape) + [5])

class DenseClassifier(torch.nn.Module):
    def __init__(self, hdim=10):
        super(DenseClassifier, self).__init__()
        self.linear1 = torch.nn.Linear(768, hdim)
        self.linear3 = torch.nn.Linear(hdim, 5)

    def forward(self, x):
        #x = torch.Tensor(x) # if type(x) == np.ndarray else x # maybe convert to an autograd variable
        h1 = torch.sigmoid(self.linear1(x))
        h3 =  self.linear3(h1)
        #print(h3.shape)
        h3 = F.log_softmax(h3, dim=-1)
        #print(res.shape)
        return h3


def main():

    '''
    train a trainsformer, above the transformer is an NN 
    '''

    # set up the workspace
    reseed = lambda: np.random.seed(seed=0) ; ms = torch.manual_seed(0) # for reproducibility
    reseed()

    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')

    '''model init'''
    model = XLNetModel.from_pretrained('xlnet-base-cased')
    model_2 = MaskedMaxPooling(1)
    model_3 = DenseClassifier()

    if use_cuda:
        model = model.to(device)
        model_2 = model_2.to(device)
        model_3 = model_3.to(device)
    if use_cuda:
        my_model  = Seq(model, MaskedMaxPooling(1), model_3).cuda(device)
    else:
        my_model = Seq(model, MaskedMaxPooling(1), model_3)

    '''
    for parameter in model.parameters():
        print(parameter.shape)
    print('-----------------------')
    for parameter in model_3.parameters():
        print(parameter.shape)
    '''

    data = pd.read_csv('../data/yelp_review_full_csv/train.csv')
    data = pd.DataFrame(data)

    data_set = Csvdataset(csv_file='../data/yelp_review_full_csv/train.csv',
                          root_dir='../data/yelp_review_full_csv/')
    dataloader = DataLoader(data_set, batch_size=10,
                            shuffle=False, num_workers=4)

    params = [model_3.parameters()]#,model.parameters()
    optimizer = torch.optim.Adam(itertools.chain(*params), lr=1e-2)
    my_model.train()
    counter=0
    plot_counter = 0
    acc_list = []
    for epoch in range(1):
        print("epoch: {}".format(epoch))
        loss_list = []

        for i_batch, sample_batched in enumerate(dataloader):
            optimizer.zero_grad()  # zero the gradient buffers
            #if use_cuda:
            #    a = 1
            label = torch.tensor(sample_batched['label']) - 1
            label = label.to(device)
            sample = sample_batched['sample']

            t_l, m_l = data_set.generate_pad_tokens_and_comp_mask_batchwise_from_list(sample, 300, tokenizer)

            assert m_l is not None
            assert m_l.shape == t_l.shape

            print(counter)
            counter+=1

            y_hat = my_model(t_l.to(device), m_l.to(device))

            loss = F.nll_loss(y_hat.transpose(1,0)[0], label).to(device)  # this is the loss function
            loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            lott_temp = loss.cpu().detach().numpy()
            loss_list.append(lott_temp)
            
            loss_list = [x.item() for x in loss_list]

            if counter % 10:
                f = plt.figure()
                plt.scatter(list(range(len(loss_list))), loss_list, color="green")
                # plt.scatter(acc_list, list(range(len(acc_list))), color="orange", label="test")
                plt.xlabel('batch')
                plt.ylabel('loss')
                plt.legend()
                # plt.show()
                f.savefig("../data/models/exp1_XL_NN_models/pics/counter_{}_XLNet_NN_loss_train_only_NN.pdf".format(counter),
                              bbox_inches='tight')

        '''
        # eval
        my_model.eval()
        acc_list_acc = []
        for i_batch, sample_batched in enumerate(dataloader):


            label = Variable(sample_batched['label'])
            sample = sample_batched['sample']
            # print(type(label), label)
            # print(type(sample),len(sample), sample)
            t_l, m_l = data_set.generate_pad_tokens_and_comp_mask_batchwise_from_list(sample, 30, tokenizer)
            assert m_l is not None
            y_hat = my_model(t_l, m_l)

            correct = sum(y_hat[:,0,:].argmax(1)[:5] == label.numpy()[:5])
            acc = (correct/label.shape[0])*100
            acc_list_acc.append(acc)


        acc_list.append(np.sum(acc_list_acc) / len(acc_list_acc))
        '''
        torch.save(my_model.state_dict(), '../data/models/exp1_XL_NN_models/model_from_epoch_train_only_NN{}.pth'.format(epoch))

        #my_model.train()

    
    f = plt.figure()
    plt.scatter(list(range(len(acc_list))), acc_list, color="green", label='train')
    #plt.scatter(acc_list, list(range(len(acc_list))), color="orange", label="test")
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.legend()
    #plt.show()
    f.savefig("../data/models/exp1_XL_NN_models/pics/all_epoch_XLNet_NN_eval_on_train_train_only_NN.pdf", bbox_inches='tight')
    exit()


if __name__== "__main__":
    main()
