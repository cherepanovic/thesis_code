import numpy as np

import torch
from pytorch_transformers import XLNetModel, XLNetTokenizer
from torch.utils.data import Dataset, DataLoader
import torch.nn.functional as F
from torch.autograd import Variable

import pandas as pd

import os, sys
sys.path.insert(0, os.path.abspath(".."))

device = None
use_cuda = torch.cuda.is_available()
if use_cuda:
    sys.path.append('/home/cherepanov/thesis/')
    device = torch.device("cuda")
else:
    sys.path.append('/home/ic/projects/thesis/thesis_code/thesis_code/')
    device = torch.device('cpu')

from auxdir.sequential import Sequential as Seq
from auxdir.poolings import MaskedMaxPooling
from data.datareader import Csvdataset 

class DenseClassifier(torch.nn.Module):
    def __init__(self, hdim=10):
        super(DenseClassifier, self).__init__()
        self.linear1 = torch.nn.Linear(768, hdim)
        self.linear3 = torch.nn.Linear(hdim, 5)

    def forward(self, x):
        x = Variable(torch.Tensor(x)) # if type(x) == np.ndarray else x # maybe convert to an autograd variable
        h1 = torch.sigmoid(self.linear1(x))
        h3 =  self.linear3(h1)
        #print(h3.shape)
        h3 = F.log_softmax(h3, dim=-1)
        #print(res.shape)
        return h3 if self.training else h3.data.numpy()



def main():

    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')

    '''model init'''
    model = XLNetModel.from_pretrained('xlnet-base-cased')
	
    #print(torch.cuda.current_device())
    #print(torch.cuda.get_device_name(0))
   
    #model = model.to(device)

    model_2 = DenseClassifier()
    #model_2 = model_2.to(device)
    
    print(torch.rand(1, device="cuda"))

    exit()
    data = pd.read_csv('../data/yelp_review_full_csv/train.csv')
    data = pd.DataFrame(data)

    data_set = Csvdataset(csv_file='../data/yelp_review_full_csv/train.csv',
                              root_dir='../data/yelp_review_full_csv/')
    dataloader = DataLoader(data_set, batch_size=20,
                                shuffle=False, num_workers=4)

    for i_batch, sample_batched in enumerate(dataloader):
        print("device: ", device)
        label = Variable(sample_batched['label'])
        sample = sample_batched['sample']
        # print(type(label), label)
        # print(type(sample),len(sample), sample)
        t_l, m_l = data_set.generate_pad_tokens_and_comp_mask_batchwise_from_list(sample, 30, tokenizer)
        assert m_l is not None
        y_hat = model(t_l.to(device), attention_mask=m_l.to(device))

        if use_cuda:
            print(torch.cuda.current_device())

if __name__== "__main__":
    main()
