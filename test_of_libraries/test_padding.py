from pytorch_transformers import XLNetModel, XLNetLMHeadModel
from auxdir.auxiliaries import cosine_similarity
from auxdir.poolings import MaxPoolingChannel
from rest.run_glue import *

tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
model = XLNetModel.from_pretrained('xlnet-base-cased')
model.eval()

'''
after padding, pooling will be applied, then distance measurement
perm_mask -> https://github.com/huggingface/pytorch-transformers/issues/1013
'''

input_ids = torch.tensor(tokenizer.encode("Hello, my dog is cute")).unsqueeze(0)  # Batch size 1
input_ids_2 = torch.tensor(tokenizer.encode("Hello, my dog is cute <pad>")).unsqueeze(0)
mask_2 = torch.ones((1, input_ids_2.shape[1], input_ids_2.shape[1]), dtype=torch.float)
mask_2[:, :, -1] = 0.0
input_ids_3 = torch.tensor(tokenizer.encode("Hello, my dog is cute <pad> <pad>")).unsqueeze(0)
mask_3 = torch.zeros((1, input_ids_3.shape[1], input_ids_3.shape[1]), dtype=torch.float)
mask_3[:, :, 0:-2] = 1

for i in range(1):
    with torch.no_grad():

        outputs = model(input_ids)
        res = MaxPoolingChannel(1)(outputs)
        outputs_2 = model(input_ids_2, attention_mask=mask_2[:, 0])
        res_2 = MaxPoolingChannel(1)(outputs_2)
        outputs_3 = model(input_ids_3, attention_mask=mask_3[:, 0])
        res_3 = MaxPoolingChannel(1)(outputs_3)

for i in range(outputs[0][0,:].shape[0]):
    print("Hello, my dog is cute/Hello, my dog is cute <pad> dim#:", i,cosine_similarity(outputs[0][0,i].numpy(),outputs_2[0][0,i].numpy()))

print('-------------------')
for i in range(outputs[0][0,:].shape[0]):
    print("Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#:", i,cosine_similarity(outputs[0][0,i].numpy(),outputs_3[0][0,i].numpy()))

print('-------------------')

for i in range(outputs_2[0][0,:].shape[0]):
    print("Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#:", i,cosine_similarity(outputs_2[0][0,i].numpy(),outputs_3[0][0,i].numpy()))

'''
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 0 0.9999999413703398
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 1 1.0000000465438699
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 2 1.000000000000007
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 3 0.9999999620304815
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 4 1.0000000000015001
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 5 0.9999999502016026
Hello, my dog is cute/Hello, my dog is cute <pad> dim#: 6 1.000000047706968
-------------------
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 0 1.0000000000000617
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 1 0.9999999534561627
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 2 1.0000000000001106
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 3 1.0000000000000115
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 4 0.9999999518847271
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 5 1.0000000000003175
Hello, my dog is cute/Hello, my dog is cute <pad> <pad> dim#: 6 1.0000000954140886
-------------------
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 0 0.999999941370278
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 1 0.999999906912401
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 2 1.000000000000062
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 3 1.000000037969543
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 4 1.00000004811622
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 5 0.9999999502025548
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 6 1.0000000477071729
Hello, my dog is cute <pad>/Hello, my dog is cute <pad> <pad> dim#: 7 1.00000000000002
'''


exit()

'''
tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
#print(tokenizer.pad_token) #output <pad>
config = XLNetConfig.from_pretrained('xlnet-base-cased') #here there is an inconsistency regarding the outputs
config.output_hidden_states=True
xlnet_model = XLNetModel(config)
xlnet_model.from_pretrained('xlnet-base-cased')
#xlnet_model = XLNetModel.from_pretrained('xlnet-base-cased')
xlnet_model.eval()
'''

'''
playing around with model, goal is to make embedding vectors for sentences, first goal is to make padding properly 

I tried also with following strings:

"<cls>this is a car<eod>" 
"<cls>this is a car <pad><eod>"
'''

#print(input_ids)   #tensor([[   17, 11368,    19,    94,  2288,    27, 10920]])
#print(input_ids_2) #tensor([[   17, 11368,    19,    94,  2288,    27, 10920,     5]])
#print(input_ids_3) #tensor([[   17, 11368,    19,    94,  2288,    27, 10920,     5,     5]])


last_hidden_states = outputs[0]
last_hidden_states_2 = outputs_2[0]
last_hidden_states_3 = outputs_3[0]

#print(last_hidden_states[0].shape) #torch.Size([7, 768])
#print(last_hidden_states_2[0].shape) #torch.Size([8, 768])
#print(last_hidden_states_3[0].shape) #torch.Size([9, 768])

x = last_hidden_states[0][1]
y = last_hidden_states_2[0][1]
z = last_hidden_states_3[0][1]

#print(x.shape) #torch.Size([768])
#print(y.shape) #torch.Size([768])
#print(z.shape) #torch.Size([768])

distance = cosine_similarity(x.detach().numpy(), y.detach().numpy())
distance2 = cosine_similarity(x.detach().numpy(), z.detach().numpy())

print(distance) #output 1.0000000465438699
print(distance2) #output 0.9999999534561627

'''
here whereby the distance of predicted words will be measured 
'''

# We show how to setup inputs to predict a next token using a bi-directional context.
input_ids = torch.tensor(tokenizer.encode("Hello, my dog is very <mask>")).unsqueeze(0)  # We will predict the masked token
perm_mask = torch.zeros((1, input_ids.shape[1], input_ids.shape[1]), dtype=torch.float)
perm_mask[:, :, -1] = 1.0  # Previous tokens don't see last token
target_mapping = torch.zeros((1, 1, input_ids.shape[1]), dtype=torch.float)  # Shape [1, 1, seq_length] => let's predict one token
target_mapping[0, 0, -1] = 1.0  # Our first (and only) prediction will be the last token of the sequence (the masked token)
outputs = model(input_ids, attention_mask=perm_mask[:, 0], perm_mask=perm_mask, target_mapping=target_mapping)
next_token_logits = outputs[0]  # Output has shape [target_mapping.size(0), target_mapping.size(1), config.vocab_size]

# We show how to setup inputs to predict a next token using a bi-directional context.
input_ids = torch.tensor(tokenizer.encode("<pad> <pad> <pad> Hello, my dog is very  <mask> <pad> <pad> <pad>")).unsqueeze(0)  # We will predict the masked token
perm_mask = torch.ones((1, input_ids.shape[1], input_ids.shape[1]), dtype=torch.float)
perm_mask[:, :, 0:-4] = 0.0
perm_mask[:, :, 0:3] = 1
target_mapping = torch.zeros((1, 1, input_ids.shape[1]), dtype=torch.float)  # Shape [1, 1, seq_length] => let's predict one token
target_mapping[0, 0, -4] = 1.0  # Our first (and only) prediction will be the last token of the sequence (the masked token)

outputs = model(input_ids, attention_mask=perm_mask[:, 0], perm_mask=perm_mask, target_mapping=target_mapping)
next_token_logits2 = outputs[0]  # Output has shape [target_mapping.size(0), target_mapping.size(1), config.vocab_size]

distance = cosine_similarity(next_token_logits2.detach().numpy().reshape(-1,1)[0], next_token_logits.detach().numpy().reshape(-1,1)[0])

print(distance) #1.0000000389892618



