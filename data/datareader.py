from torch.utils.data import Dataset, DataLoader
import pandas as pd
import numpy as np
import torch

from pytorch_transformers import XLNetTokenizer, XLNetModel

class Csvdataset(Dataset):
    def __init__(self, csv_file, root_dir, transform=None):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.data_set = pd.read_csv(csv_file)
        self.root_dir = root_dir
        self.transform = transform

    def __len__(self):
        return len(self.data_set)

    def __getitem__(self, idx):
        sample = self.data_set.iloc[idx,1]
        label = self.data_set.iloc[idx,0]
        sample = {'sample': sample, 'label': label}
        if self.transform:
            sample = self.transform(sample)
        return sample

    '''
    generate padding for an sentence and also a the mask for that sentence
    '''
    def generate_pad_tokens_and_comp_mask(self, tokens, sentence_length, length, tokenizer, pad='<pad>'):
        mask_tokens = torch.ones(length, dtype=torch.float)
        i = sentence_length
        for i in range(length - sentence_length):
            mask_tokens[sentence_length+i] = 0
        #print("length: ",length)
        #print("sentence_legth: ", sentence_length)
        #print(tokens)
        pads = torch.zeros(length - sentence_length, dtype=torch.float)
        tokens.extend([pad] * (length-sentence_length))
        tokens = tokenizer.convert_tokens_to_ids(tokens)
        return tokens, mask_tokens

    '''
    generate the padding for a batch and the mask 
    '''
    def generate_pad_tokens_and_comp_mask_batchwise(self, tokens_list, length, tokenizer, pad='<pad>'):
        res_tokens_list = []
        res_mask_list = []
        for i in tokens_list:
            t,m = self.generate_pad_tokens_and_comp_mask(i, len(i), length, tokenizer)
            res_tokens_list.append(t)
            res_mask_list.append(m.numpy())
        return torch.tensor([res_tokens_list])[0], torch.tensor([res_mask_list],dtype=torch.float)[0]

    def generate_pad_tokens_and_comp_mask_batchwise_from_list(self, liste, length, tokenizer, pad='<pad>'):
        liste = [tokenizer.tokenize(x)[:length] for x in liste]
        t_l, m_l = self.generate_pad_tokens_and_comp_mask_batchwise(liste, length, tokenizer)
        return t_l, m_l


if __name__ == '__main__':

    ### Let's load a model and tokenizer
    model = XLNetModel.from_pretrained('xlnet-base-cased')
    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')


    ### Now let's save our model and tokenizer to a directory
    model.save_pretrained('../my_saved_mode_directory/')
    tokenizer.save_pretrained('../my_saved_mode_directory/')

    ### Reload the model and the tokenizer
    model = XLNetModel.from_pretrained('../my_saved_mode_directory/')
    tokenizer = XLNetTokenizer.from_pretrained('../my_saved_mode_directory/')

    input_ids = torch.tensor(tokenizer.encode('I am a machine'))  # .unsqueeze(0)
    print(input_ids)







    exit()
    #import os
    #print(os.listdir("./yelp_review_full_csv/train.csv"))

    batch_size = 200
    sequence_length = 300
    data = pd.read_csv('./yelp_review_full_csv/train.csv')
    data = pd.DataFrame(data)
    data_set = Csvdataset(csv_file='./yelp_review_full_csv/train.csv',
                          root_dir='./yelp_review_full_csv/',
                          )
    dataloader = DataLoader(data_set, batch_size= batch_size,
                            shuffle=True, num_workers=4)

    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')

    dict = {}
    for i_batch, sample_batched in enumerate(dataloader):

        ''' here calc the average token length'''

        sample = sample_batched['sample']

        l = [len(tokenizer.tokenize(x)) for x in sample]

        for i in l:
            if i in dict:
                dict[i] = dict[i] + 1
            else:
                dict[i] = 1


    import matplotlib.pyplot as plt

    plt.bar(list(dict.keys()), dict.values(), color='g')
    plt.xlabel('number of tokens')
    plt.ylabel('# of the same length')
    plt.show()
    ''' end here calc the average token length'''



    '''


    exit()
        if(len(sample_batched['label']) < batch_size):
            continue

        tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
        input_ids = torch.tensor(tokenizer.encode('I am a machine'))#.unsqueeze(0)

        print(input_ids)
        print(input_ids.unsqueeze(0))

        to_send = tokenizer.tokenize('I am')
        tokens, mask = data_set.generate_pad_tokens_and_comp_mask(to_send, len(to_send), 10, tokenizer)
        print(mask)

        to_send =[tokenizer.tokenize(x) for x in ['Moritz is', 'Moritz is going']]
        print(to_send)
        t_l, m_l = data_set.generate_pad_tokens_and_comp_mask_batchwise(to_send, 10, tokenizer)
        print(t_l)
        print(m_l)



        exit()
        example = ['Moritz is <pad> <pad> <pad> <pad> <pad> <pad> <pad>', 'Moritz is going <pad> <pad> <pad> <pad> <pad> <pad>']
        example = [tokenizer.encode(x) for x in example]

        model = XLNetModel.from_pretrained('xlnet-base-cased')
        model.eval()

        bla = torch.tensor([example[0]])
        bla2 = torch.tensor([example[1]])


        res = model(bla, attention_mask=torch.tensor([[1, 1, 1, 0, 0, 0, 0, 0, 0, 0]], dtype=torch.float))[0].detach()
        res_2 = model(bla2, attention_mask=torch.tensor([[1, 1, 1, 1, 0, 0, 0, 0, 0, 0]], dtype=torch.float))[0].detach()

        res_3 = model(t_l,attention_mask=m_l)

        print(res_3[0].shape)

        from auxdir.auxiliaries import cosine_similarity
        for x in range(res_3[0].shape[0]):
            print(res_3[0][x].shape)
            if x == 0:
                for y in range(res_3[0][x].shape[0]):
                    #print('a',res[x][y].shape)
                    #print('b',res_3[0][x][y].shape)
                    print(res_3[0][x][y][0].detach().numpy())
                    print(res[0][y][0].numpy())
                    print('<->',cosine_similarity(res_3[0][x][y].detach().numpy(),res[0][y].numpy()))
                    print('<s>', cosine_similarity(res[0][y].numpy(), res[0][y].numpy()))
                    print('<s>', cosine_similarity(res_3[0][x][y].detach().numpy(), res_3[0][x][y].detach().numpy()))
            if x == 1:
                for y in range(res_3[0][x].shape[0]):
                    print(cosine_similarity(res_3[0][x][y].detach().numpy(),res_2[0][y].numpy()))
                    print(res_3[0][x][y][0].detach().numpy())
                    print(res_2[0][y][0].numpy())
                    print('<s>', cosine_similarity(res_2[0][y].numpy(), res_2[0][y].numpy()))
                    print('<s>', cosine_similarity(res_3[0][x][y].detach().numpy(), res_3[0][x][y].detach().numpy()))
            
            for i in range(res_3[x][0][0, :].shape[0]):
                # print("outputs/outputs_3 #:", i,cosine_similarity(outputs[0][0,i].numpy(),outputs_3[0][0,i].numpy()))
                # print(i, outputs[0][0,i][0])
                print(i, res_3[x][0][0][i, 0].item())
                if x == 0:
                    print(i, res[0][0][i, 0].item())
                if x == 1:
                    print(i, res_2[0][0][i, 0].item())
       
        exit()

        '''


