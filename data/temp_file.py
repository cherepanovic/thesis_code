import spacy
import torch
from numpy.testing import assert_almost_equal


nlp = spacy.load("en_pytt_xlnetbasecased_lg")
doc = nlp("Here is some text to encode.")

assert doc.tensor.shape == (7, 768)  # Always has one row per token
doc._.pytt_word_pieces_  # String values of the wordpieces
doc._.pytt_word_pieces  # Wordpiece IDs (note: *not* spaCy's hash values!)
doc._.pytt_alignment  # Alignment between spaCy tokens and wordpieces
# The raw transformer output has one row per wordpiece.

assert len(doc._.pytt_last_hidden_state) == len(doc._.pytt_word_pieces)
# To avoid losing information, we calculate the doc.tensor attribute such that
# the sum-pooled vectors match (apart from numeric error)

#assert_almost_equal(doc.tensor.sum(axis=1), doc._.pytt_last_hidden_state.sum(axis=1))
span = doc[2:4]

# Access the tensor from Span elements (especially helpful for sentences)
#assert span.tensor == doc.tensor[2:4]
# .vector and .similarity use the transformer outputs
apple1 = nlp("Apple shares rose on the news. <pad>")

apple2 = nlp("Apple shares rose on the news.")
apple3 = nlp("Apple pie is delicious.")
print(type(apple1[0]))
print(apple1.similarity(apple2))  # 1.0000001008963793
print(apple1.similarity(apple3))  # 0.97921276

