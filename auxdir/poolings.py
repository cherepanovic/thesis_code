import torch

class MaxPoolingChannel(torch.nn.AdaptiveMaxPool1d):
    def forward(self, input):
        input = input[0]
        input = input.transpose(1,2)
        result =  torch.nn.AdaptiveMaxPool1d(1)(input)
        return result.transpose(2,1)


class AveragePoolingChannel(torch.nn.AdaptiveMaxPool1d):
    def forward(self, input):
        input = input[0]
        input = input.transpose(1, 2)
        result = torch.nn.functional.adaptive_avg_pool1d(input, self.output_size)
        return result.transpose(2, 1)

class MaskedMaxPooling(torch.nn.AdaptiveMaxPool1d):
    def forward(self, input, attention_mask=None):
        input = input[0]

        #print("input", input)
        #print("attention_mask", attention_mask)
        for i in range(input.shape[0]):
            index = torch.tensor(attention_mask[i].sum(), dtype=torch.int32)
            input[i,index:].zero_()

        input = input.transpose(1,2)
        result =  torch.nn.AdaptiveMaxPool1d(1)(input)
        return result.transpose(2,1)


if __name__ == '__main__':
    a = torch.tensor([[[2,3,4,2,1],[1,2,3,4,5],[3,4,1,9,5],[5,6,1,2,9]],[[2,3,3,2,1],[1,2,3,4,5],[10,8,1,9,5],[9,9,9,9,9]]], dtype=torch.float)
    mpool = MaskedMaxPooling(1)
    print(mpool((a,1), torch.tensor([[1,1,0,0],[1,1,1,1]])).shape)


    #test