import numpy as np

def cosine_similarity(a,b):
    return np.dot(a,b) / ( (np.dot(a,a) **.5) * (np.dot(b,b) ** .5) )


l = 0.003

def rbf(x, y):
    res = np.exp(-l * np.subtract.outer(x, y) ** 2)
    return res

def rbf_wrt_x_star(x_star, y):
    K = rbf(x_star, y)
    print(y)
    print(x_star)
    part_sec = y-x_star
    print(part_sec)
    res = K * part_sec
    return res / l
