import warnings
from collections import OrderedDict
from torch._six import container_abcs
from itertools import islice
import operator

import torch


class Sequential(torch.nn.Module):
    def __init__(self, *args):
        super(Sequential, self).__init__()
        if len(args) == 1 and isinstance(args[0], OrderedDict):
            for key, module in args[0].items():
                print(key)
                self.add_module(key, module)
        else:
            for idx, module in enumerate(args):
                self.add_module(str(idx), module)
        self.counter = 0

    def _get_item_by_idx(self, iterator, idx):
        """Get the idx-th item of the iterator"""
        size = len(self)
        idx = operator.index(idx)
        if not -size <= idx < size:
            raise IndexError('index {} is out of range'.format(idx))
        idx %= size
        return next(islice(iterator, idx, None))

    def __getitem__(self, idx):
        if isinstance(idx, slice):
            return self.__class__(OrderedDict(list(self._modules.items())[idx]))
        else:
            return self._get_item_by_idx(self._modules.values(), idx)

    def __setitem__(self, idx, module):
        key = self._get_item_by_idx(self._modules.keys(), idx)
        return setattr(self, key, module)

    def __delitem__(self, idx):
        if isinstance(idx, slice):
            for key in list(self._modules.keys())[idx]:
                delattr(self, key)
        else:
            key = self._get_item_by_idx(self._modules.keys(), idx)
            delattr(self, key)

    def __len__(self):
        return len(self._modules)

    def __dir__(self):
        keys = super(Sequential, self).__dir__()
        keys = [key for key in keys if not key.isdigit()]
        return keys

    def forward(self, input, attention_mask):
        at = attention_mask
        self.counter = 0
        for module in self._modules.values():
            if self.counter == 0:
                input = module(input, attention_mask=at)
            elif self.counter == 1:
                input = module(input, attention_mask=at)
            else:
                input = module(input)
            self.counter +=1
            # exit()
        return input

if __name__ == '__main__':
    from pytorch_transformers import XLNetModel, XLNetLMHeadModel
    from auxdir.auxiliaries import cosine_similarity
    from auxdir.poolings import MaxPoolingChannel
    from rest.run_glue import *


    tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased')
    model = XLNetModel.from_pretrained('xlnet-base-cased')
    model.eval()

    torch_seq = torch.nn.Sequential(model, MaxPoolingChannel(1))
    my_seq  = Sequential(model, MaxPoolingChannel(1))

    input_ids = torch.tensor(tokenizer.encode("Hello, my dog is cute")).unsqueeze(0)  # Batch size 1

    input_ids_3 = torch.tensor(tokenizer.encode("Hello, my dog is cute<pad><pad>")).unsqueeze(0)
    mask_3 = torch.zeros((1, input_ids_3.shape[1], input_ids_3.shape[1]), dtype=torch.float)
    mask_3[:, :, 0:-2] = 1  # Previous tokens don't see last token


    for i in range(1):
        with torch.no_grad():

            outputs = model(input_ids_3, attention_mask=mask_3[:, 0])
            res = MaxPoolingChannel(1)(outputs)


            print(mask_3[:, 0])
            res_2 = my_seq(input_ids_3, attention_mask = mask_3[:, 0])

            print(cosine_similarity(res.detach().numpy()[0][0], res_2.detach().numpy()[0][0]))
            print(cosine_similarity(res_2.detach().numpy()[0][0], res_2.detach().numpy()[0][0]))
            print(cosine_similarity(res.detach().numpy()[0][0], res.detach().numpy()[0][0]))

