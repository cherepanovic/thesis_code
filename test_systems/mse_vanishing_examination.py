from auxdir.auxiliaries import *
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import sys
import random
np.set_printoptions(threshold=sys.maxsize)


X = np.arange(0,100)
Y = random.sample(range(1, 100), 200)

res = rbf(X,Y)
res = rbf_wrt_x_star(X,Y)

print(res)
ax = sns.heatmap(res)
plt.show()
